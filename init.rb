require 'redmine'
require './plugins/redmine_bitbucket_git/app/controllers/bitbucketgit_hook_controller.rb'

Redmine::Plugin.register :redmine_bitbucket_git do
  name 'GIT Bitbucket Hook plugin'
  author 'Bastian Bringenberg'
  description 'This plugin allows your Redmine installation to receive Bitbucket GIT post-receive notifications. Based on bitbucket plugin by Alessio Caiazza and github work by Jakob Skjerning.'
  version '0.1.0'
  settings :default => { :git_dir  => '' }, :partial => 'settings/bitbucketgit_hook_setting'
end

require './plugins/redmine_bitbucket_git/app/models/repository_observer.rb'
ActiveRecord::Base.observers << :repository_observer
