class RepositoryObserver < ActiveRecord::Observer
  # CREATE DIR Manually
  # KNOWN Problem: Changing BitBucket Git Dir in use
  # KNOWN Problem: Two Repositories with the same name from different users

  def before_save(repository)
    if repository.type == 'Repository::Git' && repository.url =~ /bitbucket\.org/
      bitbucket_url = repository.url
      repo_name = File.basename(bitbucket_url)
      git_dir = Setting.plugin_redmine_bitbucket_git[:bitbucketgit_dir]
      git_dir = 'bitbucket_git_repositories' if git_dir.blank?
      repo_path = File.join(Dir.getwd, git_dir, repo_name)
      logger.debug { "repo_path = '#{repo_path}'" }
      if File.exist?(repo_path)
        exec('git clone --mirror ' + bitbucket_url + ' "' + repo_path + '"')
        repository.url = repo_path
      else
        logger.debug { "Dir already in use" }
        return false
      end
    end
  end

  private

  def exec(command)
    logger.debug { "Executing command: '#{command}'" }
    output = `#{command}`
    logger.debug { "Command result: '#{output}'" }
  end

end
