require 'json'
# Update by Bastian Bringenberg <typo3@bastian-bringenberg.de> 2012

class BitbucketgitHookController < ApplicationController

  skip_before_filter :verify_authenticity_token, :check_if_login_required

  def index
    if params[:payload].nil?
      render :text => 'nothing to do'
      return
    end

    payload = JSON.parse(params[:payload])
    logger.debug { "Received POST from Bitbucket: #{payload.inspect}" }

    project_id = payload['project_id']
    repository_id = payload['repository_id']

    raise ArgumentError, 'Project identifier not specified in POST' unless project_id
    raise ArgumentError, 'Repository identifier not specified in POST' unless repository_id

    project = Project.find_by_identifier(project_id)
    repository = Repository.find_by_identifier(repository_id)

    raise ArgumentError, "Project not found by identifier: '#{project_id}'" unless project
    raise ArgumentError, "Repository not found by identifier: '#{repository_id}'" unless repository
    raise ArgumentError, 'Repository is not a Git repository' unless repository.is_a?(Repository::Git)
    raise ArgumentError, 'Repository is not associated with Project' unless project.repositories.include?(repository)

    if Directory.exist?(repository.url)
      command = "cd \"#{repository.url}\" && git fetch"
    else
      logger.info { "Repository directory does not exist: #{repository.url}" }
      return
    end

    logger.debug { "Executing command: '#{command}'" }
    exec(command)

    # Fetch the new changesets into Redmine
    repository.fetch_changesets

    render :text => 'OK'
  end

  private

  def exec(command)
    logger.debug { "Executing command: '#{command}'" }
    output = `#{command}`
    logger.debug { "Command result: '#{output}'" }
  end

end
